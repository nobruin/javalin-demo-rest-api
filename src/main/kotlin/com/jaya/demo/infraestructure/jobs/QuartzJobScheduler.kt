package com.jaya.demo.infraestructure.jobs


import org.quartz.DateBuilder
import org.quartz.JobBuilder
import org.quartz.Scheduler
import org.quartz.SchedulerFactory
import org.quartz.TriggerBuilder
import org.quartz.impl.StdSchedulerFactory
import org.slf4j.LoggerFactory
import java.util.Date


class QuartzJobScheduler {

    fun start() {

        val log = LoggerFactory.getLogger(QuartzJobScheduler::class.java)

        log.info("----initialize scheduler obj ----")

        val sf: SchedulerFactory = StdSchedulerFactory()
        val sched: Scheduler = sf.scheduler

        val runtime = DateBuilder.evenMinuteDate(Date())

        log.info("------- Scheduling Job  -------------------")

        val job = JobBuilder.newJob(HelloJob::class.java)
            .withIdentity("job1", "group1")
            .build()

        val trigger = TriggerBuilder
            .newTrigger()
            .withIdentity("trigger1", "group1")
            .startAt(runtime)
            .build()

        log.info("-------start schedule-----")

        sched.scheduleJob(job, trigger)
        sched.start()


        log.info("------- Waiting 65 seconds... -------------")
        try {
            Thread.sleep(65L * 1000L)
        } catch (e: Exception) {
            log.debug(e.message)
        }


        log.info("------- Shutting Down ---------------------");
        sched.shutdown(true);
        log.info("------- Shutdown Complete -----------------");
    }
}