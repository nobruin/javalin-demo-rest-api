package com.jaya.demo.infraestructure.jobs

import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException
import org.slf4j.LoggerFactory
import java.util.*


class HelloJob : Job {

    @Throws(JobExecutionException::class)
    override fun execute(context: JobExecutionContext) {
        logger?.info("Hello World! - " + Date())
    }

    companion object {
        private val logger: org.slf4j.Logger? = LoggerFactory.getLogger(HelloJob::class.java)
    }
}


