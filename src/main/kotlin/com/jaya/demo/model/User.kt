package com.jaya.demo.model

data class User(
    val name: String,
    val email: String,
    val id: Int
)