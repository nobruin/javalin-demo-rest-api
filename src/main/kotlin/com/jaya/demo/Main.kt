package com.jaya.demo

import com.jaya.demo.dao.UserDao
import com.jaya.demo.infraestructure.jobs.QuartzJobScheduler
import com.jaya.demo.model.User
import io.javalin.Javalin

fun main(){
    startJavalin()
    startSchedule()
}

fun startSchedule(){
  val scheduler = QuartzJobScheduler()
  scheduler.run()
}

fun startJavalin(){

    val userDao = UserDao()

    val app = Javalin.create().apply {
        exception(Exception::class.java){ e, _ -> e.printStackTrace()}
        error(404) {ctx -> ctx.json("not found")}
    }.start(7000)

    app.get("/users"){
            ctx -> ctx.json(userDao.users)
    }

    app.get("/users/{userId}") {ctx ->
        ctx.json(userDao.findById(ctx.pathParam("userId").toInt())!!)
    }

    app.get("/users/email/{email}") {ctx ->
        ctx.json(userDao.findByEmail(ctx.pathParam("email"))!!)
    }

    app.post("/users"){ctx ->
        val user = ctx.bodyAsClass<User>()
        userDao.save(name = user.name, email = user.email)
        ctx.status(201)
    }

    app.patch("/users/{userId}"){  ctx ->
        val user = ctx.bodyAsClass<User>()
        userDao.update(
            id = ctx.pathParam("userId").toInt(),
            user = user
        )
        ctx.status(204)
    }

    app.delete("/users/{user-id}") { ctx ->
        userDao.delete(ctx.pathParam("user-id").toInt())
        ctx.status(204)
    }
}