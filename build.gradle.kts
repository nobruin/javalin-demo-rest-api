import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val kotlinVersion = "1.5.31"
val javalinVersion = "4.1.1"
val slf4j = "1.7.28"
val jacksonVersion = "2.10.1"
val quartzVersion = "2.3.0"
val apacheCommons = "1.6"
val log4jVersion = "2.17.2"
plugins {
    kotlin("jvm") version "1.5.31"
}

group = "com.jaya"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    // javalin framework
    implementation("io.javalin:javalin:$javalinVersion")
    //Logging
    implementation( "org.slf4j:slf4j-simple:$slf4j")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")

    //quartz
    implementation("org.quartz-scheduler:quartz:$quartzVersion")
    implementation("org.quartz-scheduler:quartz-jobs:$quartzVersion")
    implementation("org.apache.commons:commons-text:$apacheCommons")

    implementation("org.apache.logging.log4j:log4j-api:$log4jVersion")
    implementation ("org.apache.logging.log4j:log4j-core:$log4jVersion")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}